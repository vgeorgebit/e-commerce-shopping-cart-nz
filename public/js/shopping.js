/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global customer */

"use strict";
/* the if statement in the constructor is necessary because you can only declare one
 * constructor in a JavaScript class. There are some cases where we are going to need to create an
 * ‘empty’ SaleItem object prior to loading data into it so the if statement is there to stop crashes
 * from occurring when the parameters are all null
 */

class SaleItem {

    constructor(product, quantity) {
        // only set the fields if we have a valid product
        if (product) {
            this.product = product;
            this.quantityPurchased = quantity;
            this.salePrice = product.list_price;
        }
    }

    getItemTotal() {
        return this.salePrice * this.quantityPurchased;
    }

}

/*
 * The reconstruct function is used to restore the object from the data stored in the session
 * storage. When we put an object in the session storage it is effectively converted into JSON and
 * loses all of its functions — this is a good thing as it prevents the session storage being used as
 * a vector for XSS attacks (discussed in lectures 19 and 20). When we get the object back out of
 * the session storage we get a generic object that has no functions. The Object.assign function
 * can be used to merge two objects, and can be used to merge a data-only object (that has lost
 * its functions due to being stored in the session storage) with a version that does contain valid
 * functions.
 * 
 */

class ShoppingCart {

    constructor() {
        this.items = new Array();
    }

    reconstruct(sessionData) {
        for (let item of sessionData.items) {
            this.addItem(Object.assign(new SaleItem(), item));
        }
    }

    getItems() {
        return this.items;
    }

    addItem(item) {
        this.items.push(item);
    }

    setCustomer(customer) {
        this.customer = customer;
    }

    getTotal() {
        let total = 0.0;
        for (let item of this.items) {
            total += item.getItemTotal();
        }
        return total.toFixed(2);
    }

}


/*need to declare the AngularJS module for our application
 create a new module, and load the other pluggable modules
 creates the primary object that allows us to interact with AngularJS. ngResource and
 ngStorage are the module names for the REST resource, and local storage modules
 */

var module = angular.module('ShoppingApp', ['ngResource', 'ngStorage']);

module.config(function ($sessionStorageProvider, $httpProvider) {
   // get the auth token from the session storage
   let authToken = $sessionStorageProvider.get('authToken');

   // does the auth token actually exist?
   if (authToken) {
      // add the token to all HTTP requests
      $httpProvider.defaults.headers.common.Authorization = 'Basic ' + authToken;
   }
});


/*Create the factory for the ngResource object that will get the products from the web
 service. For all intents and purposes this is a product DAO that is getting its data from the web
 service — the primary difference here is that it is using your RESTful web service to access the
 data rather than collections or JDBC */

module.factory('productDAO', function ($resource) {
    return $resource('/api/products/:id');
});

module.factory('categoryDAO', function ($resource) {
    return $resource('/api/categories/:cat');
});

module.factory('registerDAO', function ($resource) {
    return $resource('/api/register/');
});

module.factory('signInDAO', function ($resource) {
    return $resource('/api/customers/:username');
});

module.factory('saleDAO', function ($resource) {
    return $resource('/api/sales/');
});

/* This code factory creates the ShoppingCart object, and reconstructs it using 
 * the data from the session storage if necessary
 */
module.factory('cart', function ($sessionStorage) {
    let cart = new ShoppingCart();

    // is the cart in the session storage?
    if ($sessionStorage.cart) {

        // reconstruct the cart from the session data
        cart.reconstruct($sessionStorage.cart);
    }

    return cart;
});

/*The $resource object is being injected into the factory by AngularJS via the function parameter.
 The URI should look familiar since it is the same URI you have in your web service.
 Now we can add the first controller. This will be for managing products*/

module.controller('ProductController', function (productDAO, categoryDAO) {
    // load the products
    this.products = productDAO.query(); //method sends a GET request to the resource
    this.categories = categoryDAO.query();

    this.selectCategory = function (selectedCat) {
        this.products = categoryDAO.query({"cat": selectedCat});

    };

    this.allProd = function (selectAllProd) {
        this.products = productDAO.query({"all": selectAllProd});
    };

});

module.controller('CustomerController', function (registerDAO, signInDAO, $sessionStorage, $window, $http) {
    this.signInMessage = "Please sign in to continue.";

    this.registerCustomer = function (customer) {
        registerDAO.save(null, customer);
        $window.location.href = 'login.html';
    };

    // alias 'this' so that we can access it inside callback functions
    let ctrl = this;
    this.signIn = function (username, password) {

        // generate authentication token
        let authToken = $window.btoa(username + ":" + password);

        // store token
        $sessionStorage.authToken = authToken;

        // add token to the sign in HTTP request
        $http.defaults.headers.common.Authorization = 'Basic ' + authToken;

        // get customer from web service

        signInDAO.get({'username': username},
                // success
                        function (customer) {
                            // also store the retrieved customer
                            $sessionStorage.customer = customer;
                            // redirect to home
                            $window.location.href = '.';
                        },
                        // fail
                                function () {
                                    ctrl.signInMessage = 'Sign in failed. Please try again.';
                                }
                        );
                    };

            this.checkSignIn = function (customer) {

                if ($sessionStorage.customer) {
                    this.signedIn = true;
                    this.welcome = "Welcome " + $sessionStorage.customer.firstName + " " + $sessionStorage.customer.surname;
                } else {
                    this.signedIn = false;
                }
            };

            this.signOut = function (customer) {
                $sessionStorage.$reset();
                $window.location.href = '.';
            };
        });

module.controller('CartController', function (cart, $sessionStorage, $window, saleDAO) {

    this.items = cart.getItems();
    this.total = cart.getTotal();


    this.selectedProduct = $sessionStorage.selectedProduct;


    this.getProduct = function (selectedProduct) {

        $sessionStorage.selectedProduct = selectedProduct;
        $window.location.href = 'purchase.html';

    };

    this.addToCart = function (quantityPurchased) {
        //Get the selected product from the session storage.

        let saleItem = new SaleItem(this.selectedProduct, quantityPurchased);

        cart.addItem(saleItem);

        $sessionStorage.cart = cart;

        $window.location.href = 'cart.html';

    };

    this.checkOut = function (customer) {
        
        if(cart.getItems().length === 0) {
            alert("Please select an item to purchase!");
        } else {

        customer = $sessionStorage.customer;

        cart.setCustomer(customer);

        saleDAO.save(null, cart);

        delete $sessionStorage.cart;

        $window.location.href = 'orderconfirm.html';
    }


    };

});