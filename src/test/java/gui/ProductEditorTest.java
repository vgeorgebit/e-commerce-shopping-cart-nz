package gui;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import dao.ProductInterface;
import domain.Product;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.TreeSet;
import org.assertj.swing.core.Robot;
import org.assertj.swing.core.BasicRobot;
import org.assertj.swing.fixture.DialogFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

/**
 * A class for testing the project's ProductEntry GUI.
 *
 * @author vg
 */
public class ProductEditorTest {
    
    private ProductInterface dao;
    private DialogFixture fixture;
    private Robot robot;
    
    
    @Before
    public void setUp() {
        robot = BasicRobot.robotWithNewAwtHierarchy();
        
        // Slow down the robot a little bit - default is 30 (milliseconds).
        // Do NOT make it less than 5 or you will have thread-race problems.
        robot.settings().delayBetweenEvents(75);
        
        // add some majors for testing with
        Collection<String> categories = new TreeSet<>();
        categories.add("cat1");
        categories.add("cat2");
        
        // create a mock for the DAO
        dao = mock(ProductInterface.class);
        // stub the getCategories method to return the test categories
        when(dao.getCategories()).thenReturn(categories);
    
    }
    
    @After
    public void tearDown() {
        fixture.cleanUp();
    }
    
    
 
    @Test
    public void testSave() {
        // create the dialog passing in the mocked DAO
        ProductEditor dialog = new ProductEditor(null,true,dao);

        // use AssertJ to control the dialog
        fixture = new DialogFixture(robot,dialog);
        fixture.show().requireVisible();
        
        // enter some details into the UI components
        fixture.textBox("txtID").enterText("1");
        fixture.textBox("txtName").enterText("prodname1");
        fixture.textBox("txtArea").enterText("prod_description1");
        fixture.comboBox("txtComboBox").selectItem("cat1");
        fixture.textBox("txtPrice").enterText("1.00");
        fixture.textBox("txtQtyStock").enterText("1");
        
        // click the save button
        fixture.button("SaveButton").click();
        
        // create a Mockito argument captor to use to retrieve the passed product from the mocked DAO
        ArgumentCaptor<Product> argument = ArgumentCaptor.forClass(Product.class);
        
        // verify that the DAO.addProducts method was called, and capture the passed product
        verify(dao).addProducts(argument.capture());
        
        // retrieve the passed student from the captor
        Product savedProduct = argument.getValue();
        
        // test that the product's details were properly saved
        assertEquals("Ensure the Product ID was saved", new Integer(1), savedProduct.getProductId());
        assertEquals("Ensure the Product name was saved", "prodname1", savedProduct.getName());
        assertEquals("Ensure the Product description was saved", "prod_description1", savedProduct.getDescription());
        assertEquals("Ensure the Product category was saved", "cat1", savedProduct.getCategory());
        assertEquals("Ensure the Product price was saved", new BigDecimal(1.00), savedProduct.getList_price());
        assertEquals("Ensure the Product quantity was saved", new Integer(1), savedProduct.getQuantity_in_stock());
           
    }
    


    
}
