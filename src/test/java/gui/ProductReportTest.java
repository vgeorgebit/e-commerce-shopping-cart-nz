/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import dao.ProductInterface;
import domain.Product;
import gui.helpers.SimpleListModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import org.assertj.swing.core.BasicRobot;
import org.assertj.swing.core.Robot;
import org.assertj.swing.fixture.DialogFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 *
 * @author vg
 */
public class ProductReportTest {
    private ProductInterface dao;
    private DialogFixture fixture;
    private Robot robot;
    private Product prodOne;
    private Product prodTwo;
    
    public ProductReportTest() {
    }
    
    @Before
    public void setUp() {
        robot = BasicRobot.robotWithNewAwtHierarchy();
        robot.settings().delayBetweenEvents(75);
        
        this.prodOne = new Product(1, "name1", "description1", "category1", new BigDecimal(11.00), 22);
        this.prodTwo = new Product(2, "name2", "description2", "category2", new BigDecimal(33.00), 44);
        
        Collection<Product> products = new ArrayList<>();
        products.add(prodOne);
        products.add(prodTwo);

        Collection<String> categories = new ArrayList<>();
        categories.add("category1");
        categories.add("category2");

        dao = mock(ProductInterface.class);
        when(dao.getProducts()).thenReturn(products);
        when(dao.getCategories()).thenReturn(categories);

    }
    
    @Test
    public void testDisplay() {
        ProductReport dialog = new ProductReport(null, true, dao);
        fixture = new DialogFixture(robot, dialog);
        fixture.show().requireVisible();

        // get the model
        SimpleListModel model = (SimpleListModel) fixture.list("jlistProd").target().getModel();
                
        //assertEquals("Ensure Categories set to All", "All", fixture.comboBox("txtProdComboBox").selectedItem()); 
        
        // check the contents
        assertTrue("list contains the expected product", model.contains(prodOne));
        assertTrue("list contains the expected product", model.contains(prodTwo));
        assertEquals("list contains the correct number of products", 2, model.getSize());

        
        verify(dao, atLeast(1)).getProducts();
    }
    
    @After
    public void tearDown() {
        fixture.cleanUp();
    }
    
}
