package dao;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import domain.Product;
import java.math.BigDecimal;
import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vg
 */
public class DaoTest {
    
    //ProductInterface dao = new ProductStore();
    ProductInterface dao = new ProductManageDB("jdbc:h2:tcp://localhost:9085/project-testing");
    private Product prodOne;
    private Product prodTwo;
    private Product prodThree;
    private Product prodFour;
    
    public DaoTest() {
    }
    
    @Before
    public void setUp() {
        this.prodOne = new Product(1, "name1", "description1", "category1", new BigDecimal("11.00"), new Integer("22"));
        this.prodTwo = new Product(2, "name2", "description2", "category2", new BigDecimal("33.00"), new Integer("44"));
        this.prodThree = new Product(3, "name3", "description3", "category1", new BigDecimal("55.00"), new Integer("66"));
        
        // save the products
        dao.addProducts(prodOne);
        dao.addProducts(prodTwo);
        dao.addProducts(prodThree);
    }
    
    @Test
    public void addProd_DeleteProd_DaoTest() {
        //create a new product
        this.prodFour = new Product(4, "name4", "description4", "category4", new BigDecimal("77.00"), new Integer("88"));
        // add/save the product using DAO
        dao.addProducts(prodFour);
        
        // retrieve the same product via DAO
        Product retrieved = dao.searchId(4);
        
        //ensure that the product we saved is the one we got back
        assertEquals("Retrieved product should be the same as prodFour", prodFour, retrieved);
        assertEquals("The product's ID should be the same as prodFour", prodFour.getProductId(), retrieved.getProductId());
        assertEquals("The product's name should be the same as prodFour", prodFour.getName(), retrieved.getName());
        assertEquals("The product's description should be the same as prodFour", prodFour.getDescription(), retrieved.getDescription());
        assertEquals("The product's category should be the same as prodFour", prodFour.getCategory(), retrieved.getCategory());
        assertEquals("The product's price should be the same as prodFour", prodFour.getList_price(), retrieved.getList_price());
        assertEquals("The product's quantity should be the same as prodFour", prodFour.getQuantity_in_stock(), retrieved.getQuantity_in_stock());
        
        //delete the product via the DAO
        dao.deleteProduct(prodFour);
        //try to retrieve the deleted product
        retrieved = dao.searchId(4);
        //ensure that the student was not retrieved (should be null)
        assertNull("Product should no longer exist", retrieved);
        
    }
    
    @Test
    public void editProdtest() {
        prodOne.setName("prod1");
        dao.addProducts(prodOne);
        Product retrieved = dao.searchId(1);
        
        //ensure that prodOne's name is now changed to prod1
        assertEquals("prodOne name should now be called prod1", "prod1", retrieved.getName());
        
    }
    
    @Test
    public void getAllProdTest() {
        Collection<Product> products = dao.getProducts();
        // ensure the result includes the two saved products
        assertTrue("prodOne should exist", products.contains(prodOne));
        assertTrue("prodTwo should exist", products.contains(prodTwo));
        // ensure the result ONLY includes the two saved products
        assertEquals("Only 3 products in result", 3, products.size());
        // find prodOne - result is not a map, so we have to scan for it
        for (Product p : products) {
            if (p.equals(prodOne)) {
        // ensure that all of the details were correctly retrieved
                assertEquals(prodOne.getProductId(), p.getProductId());
                assertEquals(prodOne.getName(), p.getName());
                assertEquals(prodOne.getDescription(), p.getDescription());
                assertEquals(prodOne.getCategory(), p.getCategory());
                assertEquals(prodOne.getList_price(), p.getList_price());
                assertEquals(prodOne.getQuantity_in_stock(), p.getQuantity_in_stock());
            }
        }
    }
    
    @Test
    public void FindProdByIdTest() {
    // get prodOne using findById method
        Product retrieved = dao.searchId(1);  
    // assert that you got back prodOne, and not another product
        assertEquals("The product should be product 1", retrieved, prodOne);
    // assert that prodOne's details were properly retrieved
        assertEquals("The product's ID should be the same as Product 1's ID", prodOne.getProductId(), retrieved.getProductId());
    // call getById using a non-existent ID
        Product retrieved_null = dao.searchId(5);
    // assert that the result is null
        assertEquals("The product ID does not exist", retrieved_null, null);
    }
    
    @Test
    public void getAllCatTest() {
        
        Collection<String> categories = dao.getCategories();
        
        // ensure the result includes the categories
        assertTrue("Category category1 should exist", categories.contains("category1"));
        assertTrue("Category category2 should exist", categories.contains("category2"));

        // ensure that the size of the total categories is corrext
        assertEquals("Two Categories exist", 2, categories.size());
           
    }
    
    @Test
    public void productsByCatTest() {

        Collection<Product> products = dao.filterCategory("category1");

        // ensure the result includes the products filtered by Category
        assertTrue("Category1 should contain prodOne", products.contains(prodOne));
        assertTrue("Category1 should contain prodThree", products.contains(prodThree));
        // ensure that the total products retrieved by Category matches
        assertEquals("Category1 should only contain 2 products", 2, products.size());

        for (Product p : products) {
            if (p.equals(prodOne)) {
                // ensure that all of the details were correctly retrieved
                assertEquals("Product ID should be the same as prodOne", prodOne.getProductId(), p.getProductId());
                assertEquals("Product name should be the same as prodOne", prodOne.getName(), p.getName());
                assertEquals("Product description should be the same as prodOne", prodOne.getDescription(), p.getDescription());
                assertEquals("Product category should be the same as prodOne", prodOne.getCategory(), p.getCategory());
                assertEquals("Product price should be the same as prodOne", prodOne.getList_price(), p.getList_price());
                assertEquals("Product quantity should be the same as prodOne", prodOne.getQuantity_in_stock(), p.getQuantity_in_stock());
            } else if (p.equals(prodThree)) {
                // ensure that all of the details were correctly retrieved
                assertEquals("Product ID should be the same as prodThree", prodThree.getProductId(), p.getProductId());
                assertEquals("TProduct name should be the same as prodThree", prodThree.getName(), p.getName());
                assertEquals("Product description should be the same as prodThree", prodThree.getDescription(), p.getDescription());
                assertEquals("Product category should be the same as prodThree", prodThree.getCategory(), p.getCategory());
                assertEquals("Product price should be the same as prodThree", prodThree.getList_price(), p.getList_price());
                assertEquals("Product quantity should be the same as prodThree", prodThree.getQuantity_in_stock(), p.getQuantity_in_stock());
            }

        }
    }


    @After
    public void tearDown() {
        dao.deleteProduct(prodOne);
        dao.deleteProduct(prodTwo);
        dao.deleteProduct(prodThree);
        
    }
    
}
