/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domain.Product;
import java.util.Collection;

/**
 *
 * @author vg
 */
public interface ProductInterface {

    public void addProducts(Product product);

    //delete a product
    public void deleteProduct(Product product);

    public Collection<Product> filterCategory(String category);

    //getter for product categores
    public Collection<String> getCategories();

    public Collection<Product> getProducts();

    //method to the DAO class for performing the search. It will take a product ID
    //as a parameter and return a product.
    /**
     *
     * @param productId
     * @return
     */
    public Product searchId(Integer productId);

    //setter for product categories
    public void setCategories(String category);

}
