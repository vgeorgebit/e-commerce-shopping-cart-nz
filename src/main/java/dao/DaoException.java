/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author vg Runtime exceptions are not checked exceptions. This means that the
 * compiler will not force users of our API to pointlessly catch the exception
 * any time they use our DAO API. By using runtime exceptions we are giving the
 * API users more choice in how to deal with the exceptions — they have the
 * option to let the exception bubble up the call stack to be handled by the
 * code that makes the most sense to actually deal with the exception.
 */
public class DaoException extends RuntimeException {

    //default constructor
    public DaoException() {
    }

    //use this constructor when we detect an undesirable state in the DAO that
    //requires us to cancel the current operation by creating and throwing an exception
    public DaoException(String reason) {
        super(reason);
    }

    //use this constructor when we catch an exception that is thrown by our
    //persistence mechanism (such as JDBC)
    public DaoException(String reason, Throwable cause) {
        super(reason, cause);
    }

}
