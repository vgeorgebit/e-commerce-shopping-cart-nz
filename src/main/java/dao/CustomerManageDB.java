/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domain.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import web.auth.CredentialsValidator;

/**
 *
 * @author vg
 */
public class CustomerManageDB implements CustomerDAO, CredentialsValidator  {

    private String dbConnection = "jdbc:h2:tcp://localhost:9085/project;IFEXISTS=TRUE";

    //default constructor
    public CustomerManageDB() {
    }

    //this constructor gives us a
    //way to specify a different database.    
    public CustomerManageDB(String url) {
        this.dbConnection = url;
    }

    @Override
    public void addCustomer(Customer customer) {
        String sql = "INSERT INTO customer (username, firstName, surname, emailAddress, shippingAddress, creditCardDetails, password) VALUES (?,?,?,?,?,?,?)";
        try (Connection dbCon = JdbcConnection.getConnection(dbConnection);
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            stmt.setString(1, customer.getUsername());
            stmt.setString(2, customer.getFirstName());
            stmt.setString(3, customer.getSurname());
            stmt.setString(4, customer.getEmail());
            stmt.setString(5, customer.getAddress());
            stmt.setString(6, customer.getCreditCard());
            stmt.setString(7, customer.getPassword());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    @Override
    public Customer getCustomer(String userName) {
        String sql = "SELECT * FROM customer WHERE (username=?)";
        try (
                Connection dbCon = JdbcConnection.getConnection(dbConnection);
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {

            stmt.setString(1, userName);

            ResultSet rs = stmt.executeQuery();

            Map<String, Customer> custUserName = new HashMap<>();

            while (rs.next()) {
                Integer personId = rs.getInt("personId");
                String username = rs.getString("username");
                String firstName = rs.getString("firstName");
                String surname = rs.getString("surname");
                String password = rs.getString("password");
                String emailAddress = rs.getString("emailAddress");
                String shippingAddress = rs.getString("shippingAddress");
                String creditCardDetails = rs.getString("creditCardDetails");

                Customer c = new Customer(personId, username, firstName, surname, password, emailAddress, shippingAddress, creditCardDetails);
                custUserName.put(c.getUsername(), c);

            }

            return custUserName.get(userName);

        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    @Override
    public Boolean validateCredentials(String username, String password) {
        String sql = "SELECT * FROM customer WHERE (username=?) AND (password=?)";
        try (
                Connection dbCon = JdbcConnection.getConnection(dbConnection);
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {

            stmt.setString(1, username);
            stmt.setString(2, password);

            ResultSet rs = stmt.executeQuery();

//            Map<String, Customer> custCredentials = new HashMap<>();
//
//            while (rs.next()) {
//                String usrName = rs.getString("username");
//                String pword = rs.getString("password");
//                
//                Customer c = new Customer(usrName, pword);
//                
//                custCredentials.put(usrName, c);
//                custCredentials.put(pword, c);
//            }
//
//            if (custCredentials.containsKey(username)) {
//                return custCredentials.get(username).getPassword().equals(password);
//            } else {
//                return false;
//            }

            return rs.next();

        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage(), ex);
        }
    }

}
