package dao;

import domain.Customer;
import domain.Product;
import domain.Sale;
import domain.SaleItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaleJdbcDAO implements SaleDAO {

    private final String url = "jdbc:h2:tcp://localhost:9085/project;IFEXISTS=TRUE";

    @Override
    public void save(Sale sale) {

        Connection con = JdbcConnection.getConnection(url);
        try {
            try (//SQL for saving Sale goes here
                    PreparedStatement insertSaleStmt = con.prepareStatement("INSERT INTO sale (date, status, personId) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
                    //SQL for saving SaleItem goes here
                    PreparedStatement insertSaleItemStmt = con.prepareStatement("INSERT INTO saleitem (saleId, productId, quantityPurchased, salePrice) VALUES (?,?,?,?)");
                    //SQL for updating product quantity goes here
                    PreparedStatement updateProductStmt = con.prepareStatement("UPDATE product SET quantity_in_stock=? WHERE productId=?");
                    ) {

                // Since saving and sale involves multiple statements across
                // multiple tables we need to control the transaction ourselves
                // to ensure our DB remains consistent.
                //
                // Turn off auto-commit which effectively starts a new transaction.
                con.setAutoCommit(false);

                Customer customer = sale.getCustomer();
                // #### save the sale ### //

                // add a date to the sale if one doesn't already exist
                if (sale.getDate() == null) {
                    sale.setDate(new Date());
                }
                
                if (sale.getStatus() == null) {
                    sale.setStatus("complete");
                }

                // convert sale date into to java.sql.Timestamp
                Date date = sale.getDate();
                Timestamp timestamp = new Timestamp(date.getTime());

                // ****
                // write code here that saves the timestamp, person ID, and
                // status in the sale table using the insertSaleStmt statement.
                // ****
                insertSaleStmt.setTimestamp(1, timestamp);
                insertSaleStmt.setString(2, sale.getStatus());
                insertSaleStmt.setInt(3,sale.getCustomer().getPersonId());
                insertSaleStmt.executeUpdate();
                con.setAutoCommit(true);

                // get the auto-generated sale ID from the database
                ResultSet rs = insertSaleStmt.getGeneratedKeys();

                Integer saleId = null;

                if (rs.next()) {
                    saleId = rs.getInt(1);
                } else {
                    throw new DaoException("Problem getting generated Sale ID");
                }

                Collection<SaleItem> items = sale.getItems();

                for (SaleItem item : items) {

                    Product product = item.getProduct();
                    

                    // ****
                    // write code here that saves the sale item
                    // using the insertSaleItemStmt statement.
                    // ****
                    insertSaleItemStmt.setInt(1, rs.getInt(1));
                    insertSaleItemStmt.setInt(2, product.getProductId());
                    insertSaleItemStmt.setInt(3, item.getQuantity_purchased());
                    insertSaleItemStmt.setBigDecimal(4, item.getSalePrice());
                    insertSaleItemStmt.executeUpdate();
                    // ****
                    // write code here that updates the product quantity using
                    // the updateProductStmt statement.
                    // ****

                    Integer currentQuantity =  new ProductManageDB().searchId(item.getProduct().getProductId()).getQuantity_in_stock();
                    updateProductStmt.setInt(1, currentQuantity - item.getQuantity_purchased());
                    updateProductStmt.setInt(2, product.getProductId());
                    updateProductStmt.executeUpdate();
                }

                // commit the transaction
                con.setAutoCommit(true);
            }
        } catch (SQLException ex) {

            Logger.getLogger(SaleJdbcDAO.class.getName()).log(Level.SEVERE, null, ex);

            try {
                // something went wrong so rollback
                con.rollback();

                // turn auto-commit back on
                con.setAutoCommit(true);

                // and throw an exception to tell the user something bad happened
                throw new DaoException(ex.getMessage(), ex);
            } catch (SQLException ex1) {
                throw new DaoException(ex1.getMessage(), ex1);
            }

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SaleJdbcDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
