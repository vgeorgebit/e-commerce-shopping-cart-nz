/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import domain.Product;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author vg
 */
public class ProductStore implements ProductInterface {

    //static - there is just one copy of the data field for all instances of the class 
    private static final Collection<Product> products = new HashSet<>();//new collection for products
    private static final Collection<String> prodCatList = new HashSet<>();//new collection for product categories
    private static final Map<Integer, Product> prodId = new HashMap<>(); // create a map of products keyed by their ID (Integer)
    private static final Multimap<String, Product> mmCatProd = HashMultimap.create(); // create a multimap of categories and products (products per category 1:M)

    //default Constructor 
    public ProductStore() {
    }

    @Override
    public void addProducts(Product product) {
        products.add(product);
        mmCatProd.put(product.getCategory(), product);
        prodId.put(product.getProductId(), product);
    }

    @Override
    public Collection<Product> getProducts() {
        return products;
    }

    //setter for product categories
    @Override
    public void setCategories(String category) {
        prodCatList.add(category);
    }

    //getter for product categores
    @Override
    public Collection<String> getCategories() {
        return prodCatList;
    }

    //delete a product
    @Override
    public void deleteProduct(Product product) {
        products.remove(product);
        prodId.remove(product.getProductId());
        mmCatProd.remove(product.getCategory(), product);
    }

    //method to the DAO class for performing the search. It will take a product ID
    //as a parameter and return a product.
    /**
     *
     * @param productId
     * @return
     */
    @Override
    public Product searchId(Integer productId) {
        if (productId == null) {
            return null;
        } else {
            return prodId.get(productId);
        }
    }

    @Override
    public Collection<Product> filterCategory(String category) {
        if (category == null) {
            return null;
        } else {
            return mmCatProd.get(category);
        }
    }

}
