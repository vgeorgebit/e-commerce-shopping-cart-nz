/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import domain.Product;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vg
 */
public class ProductManageDB implements ProductInterface {

    //URL that you used when your created the database, but with an additional
    //option to tell H2 not to create a new database if it can’t find the existing one
    private String dbConnection = "jdbc:h2:tcp://localhost:9085/project;IFEXISTS=TRUE";

    //default constructor
    public ProductManageDB() {
    }

    //We don’t want to use your existing database for testing, so this constructor gives us a
    //way to specify a different database.
    public ProductManageDB(String url) {
        this.dbConnection = url;
    }

    @Override
    public void addProducts(Product product) {
        //merge bundles these two operations,tries insert first,if product already exists (duplicate PK), update instead
        //String sql = "INSERT INTO Product (Product_ID,Name,Description,Category,List_price,Qty_in_stock) VALUES (?,?,?,?,?,?)";
        String sql = "MERGE INTO product (productId,name,description,category,list_price,quantity_in_stock) VALUES (?,?,?,?,?,?)";

        try (
            // get connection to database
            Connection dbCon = JdbcConnection.getConnection(dbConnection);
            // create the statement
            PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the product domain object into the SQL parameters
            stmt.setInt(1, product.getProductId());
            stmt.setString(2, product.getName());
            stmt.setString(3, product.getDescription());
            stmt.setString(4, product.getCategory());
            stmt.setBigDecimal(5, product.getList_price());
            stmt.setInt(6, product.getQuantity_in_stock());

            stmt.executeUpdate();  // execute the statement

        } catch (SQLException ex) {  // we are forced to catch SQLException
            // don't let the SQLException leak from our DAO encapsulation
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    @Override
    public void deleteProduct(Product product) {
        
         String sql = "DELETE FROM product WHERE productId=?";

        try (
            // get a connection to the database
            Connection dbCon = JdbcConnection.getConnection(dbConnection);
            // create the statement
            //Statement stmt = dbCon.createStatement();) 
            PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            stmt.setInt(1, product.getProductId());
            stmt.executeUpdate(); //delete statement

        } catch (SQLException ex) {  // we are forced to catch SQLException
            // don't let the SQLException leak from our DAO encapsulation
            System.err.println();
            ex.getStackTrace();
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    @Override
    public Collection<Product> filterCategory(String category) {
        String sql = "SELECT * FROM product WHERE category =?"; //+ "'"+category+"'"; //set format for database category to take in string category search
        
        try (
            // get a connection to the database
            Connection dbCon = JdbcConnection.getConnection(dbConnection);
            // create the statement
            PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // execute the query
            stmt.setString(1,category);
            ResultSet rs = stmt.executeQuery();

            // Using a multi
            Multimap<String, Product> mmCatProd = HashMultimap.create();

            // iterate through the query results
            while (rs.next()) {

                // get the data out of the query
                Integer productId = rs.getInt("productId");
                String name = rs.getString("name");
                String description = rs.getString("description");
                String cat = rs.getString("category");
                BigDecimal list_price = rs.getBigDecimal("list_price");
                Integer quantity_in_stock = rs.getInt("quantity_in_stock");

                // use the data to create a product object
                Product p = new Product(productId, name, description, cat, list_price, quantity_in_stock);
                // and put it in the collection
                mmCatProd.put(p.getCategory(), p);

            }

            return mmCatProd.get(category);

        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage(), ex);
        }

    }

    @Override
    public Collection<String> getCategories() {
        //Get all categories that have been previously saved in the product table.
        String sql = "SELECT DISTINCT category FROM product";
        try (
            // get a connection to the database
            Connection dbCon = JdbcConnection.getConnection(dbConnection);
            // create the statement
            PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // execute the query
            ResultSet rs = stmt.executeQuery();

            // Using a List to preserve the order in which the data was returned from the query.
            List<String> categories = new ArrayList<>();

            // iterate through the query results
            while (rs.next()) {

                // get the data out of the query
                String category = rs.getString("category");
                // and put it in the collection
                categories.add(category);

            }

            return categories;

        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    @Override
    public Collection<Product> getProducts() {
        //Get all products that have been previously saved in the product table.
        String sql = "SELECT * FROM product ORDER BY productId";

        try (
            // get a connection to the database
            Connection dbCon = JdbcConnection.getConnection(dbConnection);
            // create the statement
            PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // execute the query
            ResultSet rs = stmt.executeQuery();

            // Using a List to preserve the order in which the data was returned from the query.
            List<Product> products = new ArrayList<>();

            // iterate through the query results
            while (rs.next()) {

                // get the data out of the query
                Integer product_id = rs.getInt("productId");
                String name = rs.getString("name");
                String description = rs.getString("description");
                String category = rs.getString("category");
                BigDecimal list_price = rs.getBigDecimal("list_price");
                Integer qty_in_stock = rs.getInt("quantity_in_stock");

                // use the data to create a product object
                Product p = new Product(product_id, name, description, category, list_price, qty_in_stock);

                // and put it in the collection
                products.add(p);
            }

            return products;

        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    @Override
    public Product searchId(Integer productId) {

        String sql = "SELECT * FROM product WHERE productId=" + productId;

        try (
            // get a connection to the database
            Connection dbCon = JdbcConnection.getConnection(dbConnection);
            // create the statement
            PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // execute the query
            ResultSet rs = stmt.executeQuery();

            Map<Integer, Product> prodId = new HashMap<>(); // create a map of products keyed by their ID (Integer)

            // iterate through the query results
            while (rs.next()) {

                // get the data out of the query
                Integer product_id = rs.getInt("productId");
                String name = rs.getString("name");
                String description = rs.getString("description");
                String category = rs.getString("category");
                BigDecimal list_price = rs.getBigDecimal("list_price");
                Integer qty_in_stock = rs.getInt("quantity_in_stock");

                // use the data to create a product object
                Product p = new Product(product_id, name, description, category, list_price, qty_in_stock);

                // and put it in the HashMap
                prodId.put(p.getProductId(), p);
            }

            return prodId.get(productId);

        } catch (SQLException ex) {  // we are forced to catch SQLException
            // don't let the SQLException leak from our DAO encapsulation
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    @Override
    public void setCategories(String category) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
