/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  vg
 * Created: Aug 13, 2018
 */

CREATE TABLE Product (
    productId INTEGER,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(255),
    category VARCHAR(50) NOT NULL,
    list_price DECIMAL(9,2) NOT NULL,
    quantity_in_stock DECIMAL(9,2) NOT NULL,
    CONSTRAINT productId PRIMARY KEY (productId)
);

CREATE TABLE Customer (
    personId INT NOT NULL AUTO_INCREMENT, 
    username VARCHAR(50) NOT NULL UNIQUE,
    firstName VARCHAR(50) NOT NULL,
    surname VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    emailAddress VARCHAR(100) NOT NULL,
    shippingAddress VARCHAR(255) NOT NULL,
    creditCardDetails VARCHAR(255) NOT NULL,
    CONSTRAINT PK_Customer PRIMARY KEY (personId)  
);

CREATE TABLE Sale (
    saleId INT NOT NULL AUTO_INCREMENT,
    date TIMESTAMP NOT NULL,
    status VARCHAR(50),
    personId VARCHAR(50),
    CONSTRAINT PK_Sale PRIMARY KEY (saleId),
    CONSTRAINT FK_Sale_CUSTOMER FOREIGN KEY (personId) REFERENCES Customer
    
);

CREATE TABLE Saleitem (
    saleId INT NOT NULL,
    productId INT NOT NULL,
    quantityPurchased INT NOT NULL,
    salePrice DECIMAL(9,2) NOT NULL,
    CONSTRAINT FK_Saleitem_Sale FOREIGN KEY (saleId) REFERENCES Sale,
    CONSTRAINT FK_Saleitem_Product FOREIGN KEY (productId) REFERENCES Product
);

