
import dao.JdbcConnection;
import dao.ProductManageDB;
import gui.ProductAdministration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author geovi616
 */
public class Administration {
    
    private static ProductManageDB prodJdbcDao =  new ProductManageDB();

    public static void main(String[] args) {
        //create the frame instance
        ProductAdministration prodAdmin = new ProductAdministration(prodJdbcDao);
        //create the frame on the screen
        prodAdmin.setLocationRelativeTo(null);
        //show the frame
        prodAdmin.setVisible(true);

    }
}
