/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovi616
 */
public class Customer {

    private Integer personId;
    private String username;
    private String firstName;
    private String surname;
    private String password;
    private String emailAddress;
    private String shippingAddress;
    private String creditCardDetails;

    //default constructor
    public Customer() {
    }
    
    public Customer(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    
    public Customer(String username, String firstName, String surname, String password, String emailAddress, String shippingAddress, String creditCardDetails) {
        this.username = username;
        this.firstName = firstName;
        this.surname = surname;
        this.password = password;
        this.emailAddress = emailAddress;
        this.shippingAddress = shippingAddress;
        this.creditCardDetails = creditCardDetails;
    }

    public Customer(Integer personId, String username, String firstName, String surname, String password, String emailAddress, String shippingAddress, String creditCardDetails) {
        this.personId = personId;
        this.username = username;
        this.firstName = firstName;
        this.surname = surname;
        this.password = password;
        this.emailAddress = emailAddress;
        this.shippingAddress = shippingAddress;
        this.creditCardDetails = creditCardDetails;
    }
    

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public void setName(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
    }
    
    public String getName() {
        return firstName +" "+ surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return emailAddress;
    }

    public void setEmail(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getAddress() {
        return shippingAddress;
    }

    public void setAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getCreditCard() {
        return creditCardDetails;
    }

    public void setCreditCard(String creditCardDetails) {
        this.creditCardDetails = creditCardDetails;
    }

    @Override
    public String toString() {
        return "Customer{" + "personId=" + personId + ", username=" + username + ", firstName=" + firstName + ", surname=" + surname + ", "
                + "password=" + password + ", emailAddress=" + emailAddress + ", shippingAddress=" + shippingAddress + ", creditCardDetails=" + creditCardDetails + '}';
    }

}
