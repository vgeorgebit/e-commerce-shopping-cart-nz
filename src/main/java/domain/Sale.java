/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

/**
 *
 * @author vg
 */
public class Sale {


    private Integer saleID;
    private Date date;
    private String status;
    private Customer customer; //Sale will have one customer 


    private SaleItem item;
    private final Collection<SaleItem> items = new ArrayList<>(); //sale will have either zero or multiple sale items 

    // default constructor 
    
    public Sale(Date date, String status, Customer customer) {
        this.date = date;
        this.status = status;
        this.customer = customer;
    }
    
    public Sale(Customer customer) {
        this.customer = customer;
    }


    public void addItem(SaleItem saleItem) {
        this.items.add(saleItem);
    }

    public Collection<SaleItem> getItems() {
        return items;
    }

    public Customer getCustomer() {
        return customer;
    }
    

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getSaleID() {
        return saleID;
    }

    public void setSaleID(Integer saleID) {
        this.saleID = saleID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTotal() {
        BigDecimal total = new BigDecimal(0);
        {
            for (Iterator<SaleItem> it = items.iterator(); it.hasNext();) {
                item = it.next();
                total = total.add(item.getItemTotal());
            }
        }
        return total;
    }

    @Override
    public String toString() {
        return "Sale{" + "customer=" + customer + ", saleID=" + saleID + ", date=" + date + ", status=" + status + ", items=" + items + '}';
    }

}
