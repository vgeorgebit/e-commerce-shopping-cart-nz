/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.math.BigDecimal;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

/**
 *
 * @author geovi616
 */
public class Product {

    @NotNull(message = "Product ID must be provided")
    @NotBlank(message = "Product ID must be provided")
    private Integer productId;
    @NotNull(message = "Product name must be provided")
    @NotBlank(message = "Product name must be provided")
    @Length(min = 2, message = "Product name must contain at least two characters.")
    private String name;
    private String description;
    @NotNull(message = "Product category must be provided")
    @NotBlank(message = "Product category must be provided")
    @Length(min = 2, message = "Product category must contain at least two characters.")
    private String category;
    @NotNull(message = "Product price must be provided")
    private BigDecimal list_price;
    @NotNull(message = "Product quantity must be provided")
    private Integer quantity_in_stock;

    //default constructor
    public Product() {
    }

    public Product(Integer productId, String name, String description, String category, BigDecimal list_price, Integer quantity_in_stock) {
        this.productId = productId;
        this.name = name;
        this.description = description;
        this.category = category;
        this.list_price = list_price;
        this.quantity_in_stock = quantity_in_stock;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getList_price() {
        return list_price;
    }

    public void setList_price(BigDecimal list_price) {
        this.list_price = list_price;
    }

    public Integer getQuantity_in_stock() {
        return quantity_in_stock;
    }

    public void setQuantity_in_stock(Integer quantity_in_stock) {
        this.quantity_in_stock = quantity_in_stock;
    }

    @Override
    public String toString() {
        return productId + ", " + name;
        //return "Product{" + "productId=" + productId + ", name=" + name + ", description=" + description + 
        //        ", category=" + category + ", list_price=" + list_price + ", quantity_in_stock=" + quantity_in_stock + '}';

    }

}
