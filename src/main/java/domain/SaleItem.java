/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.math.BigDecimal;

/**
 *
 * @author vg
 */
public class SaleItem {


    private Sale sale; //Saleitem must have one sale
    private Product product; // Saleitem must have one product    
    private Integer quantityPurchased;
    private BigDecimal salePrice;
    private BigDecimal itemTotal;

    
    public SaleItem(Product product, Integer quantityPurchased) {
        this.product = product;
        this.quantityPurchased = quantityPurchased;
    }

    public SaleItem() {
        
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity_purchased() {
        return quantityPurchased;
    }

    public void setQuantity_purchased(Integer quantityPurchased) {
        this.quantityPurchased = quantityPurchased;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    //getItemTotal method that calculates total price of the item
    public BigDecimal getItemTotal() {
        itemTotal = salePrice.multiply(new BigDecimal(quantityPurchased));
        return itemTotal;
    }

    @Override
    public String toString() {
       return "Product Name: " + product.getName() + ", Price: $" + product.getList_price() + ", Quantity: " + quantityPurchased + ", Total: $" + getItemTotal()+"\n";
    }
}
