/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dao.ProductInterface;
import dao.ProductManageDB;
import org.jooby.Jooby;

/**
 *
 * @author vg
 */
public class ProductModule extends Jooby {
    
    ProductManageDB productDao = new ProductManageDB();

    public ProductModule(ProductInterface productDao) {
        
        get("/api/products/", () -> productDao.getProducts());
        
        get("/api/products/:id", (req) -> {
            Integer id = req.param("id").intValue();
            return productDao.searchId(id);
        });
        
        get("/api/categories/", () -> productDao.getCategories());
        
        get("/api/categories/:category", (req) -> {
            String category = req.param("category").value();
            return productDao.filterCategory(category);
        });
    }
}
