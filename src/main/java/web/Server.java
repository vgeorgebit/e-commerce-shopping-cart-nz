/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dao.CustomerDAO;
import dao.CustomerManageDB;
import dao.ProductInterface;
import dao.ProductManageDB;
import dao.SaleDAO;
import dao.SaleJdbcDAO;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.jooby.Jooby;
import org.jooby.json.Gzon;
import web.auth.BasicHttpAuthenticator;
import web.auth.CredentialsValidator;

/**
 *
 * @author vg
 */
public class Server extends Jooby {
    
    public final ProductInterface productDao;
    public final CustomerDAO customerDao;
    public final SaleDAO saleDao;

    public Server() {
        productDao = new ProductManageDB();
        customerDao = new CustomerManageDB();
        saleDao = new SaleJdbcDAO();
        port(8086);
        use(new Gzon());
        use(new AssetModule());
        List<String> noAuth = Arrays.asList("/api/register"); //omit from authentication, everything below requires auth
        use(new BasicHttpAuthenticator(customerDao, noAuth));
        use(new SaleModule(saleDao));
        use(new ProductModule(productDao));
        use(new CustomerModule(customerDao));

    }
    

    public static void main(String[] args) throws Exception {
        System.out.println("\nStarting Server.");
        Server server = new Server();
        CompletableFuture.runAsync(() -> {
            server.start();
        });
        server.onStarted(() -> {
            System.out.println("\nPress Enter to stop the server.");
        });
        // wait for user to hit the Enter key
        System.in.read();
        System.exit(0);
        

    }

}
