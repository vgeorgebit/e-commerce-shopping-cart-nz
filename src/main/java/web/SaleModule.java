/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dao.SaleDAO;
import domain.Sale;
import domain.SaleItem;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.jooby.Jooby;
import org.jooby.Status;

/**
 *
 * @author vg
 */
public class SaleModule extends Jooby {

    public SaleModule(SaleDAO saleDAO) {

        post("/api/sales/", (req, rsp) -> {
            Sale sale = req.body().to(Sale.class);

            saleDAO.save(sale);
            rsp.status(Status.CREATED);

            CompletableFuture.runAsync(() -> {
                // code to send email goes here
                try {
                    Collection<SaleItem> items = sale.getItems();
                    SimpleEmail email = new SimpleEmail();
                    email.setHostName("localhost");
                    email.setSmtpPort(2525);
                    //email.setAuthenticator(new DefaultAuthenticator("username", "password"));
                    //email.setSSLOnConnect(true);
                    email.setFrom("sales@legostore.com");
                    email.setSubject("Your LegoStore.com order has shipped!");
                    email.setMsg("Hello! " + sale.getCustomer().getName()+",\n\n" + "Your LegoStore.com order:\n\n"
                            +items.toString().replace("[", "").replace("]", "").trim()
                            +"\n\n\n" + "Total Invoice: $" + sale.getTotal()
                            +"\n\n\n" + "Thank you for shopping with us," +"\n\n" + "The LegoStore.com team!");           
                    email.addTo(sale.getCustomer().getEmail());
                    email.send();
                } catch (EmailException ex) {
                    Logger.getLogger(SaleModule.class.getName()).log(Level.SEVERE, null, ex);
                }

            });

        });

    }

}
