/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;


import dao.CustomerDAO;
import domain.Customer;
import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Status;



/**
 *
 * @author vg
 */
public class CustomerModule extends Jooby {


    public CustomerModule(CustomerDAO customerDao) {

        get("/api/customers/:username", (req) -> {
            String username = req.param("username").value();
            if(customerDao.getCustomer(username) == null){
                throw new Err(Status.NOT_FOUND); 
            } else {
                return customerDao.getCustomer(username);
            
            }   
        });

        post("/api/register/", (req, rsp) -> {
            Customer customer = req.body().to(Customer.class);
            customerDao.addCustomer(customer);
            rsp.status(Status.CREATED);
        });
    }

}
