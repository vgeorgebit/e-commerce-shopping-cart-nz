/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import org.jooby.Jooby;
import org.jooby.Results;

/**
 *
 * @author vg
 */
public class AssetModule extends Jooby {
    
    /* This assumes that you will put your HTML files in the root of the Resource [Web] folder, and
     * the CSS, JavaScript, and images in sub-folders (named css, js, and images respectively)
     */

    public AssetModule() {
        

        //assets("/*.html");
        assets("/cart.html");
        assets("/createaccount.html");
        assets("/index.html");
        assets("/login.html");
        assets("/menu.html");
        assets("/orderconfirm.html");
        assets("/purchase.html");
        assets("/viewproducts.html");
        
        assets("/css/styles.css");
        assets("/js/angular-resource.min.js");
        assets("/js/angular.min.js");
        assets("/js/ngStorage.min.js");
        assets("/js/shopping.js");
        assets("/images/*.png");
        assets("/images/*.jpg");
        assets("/images/*.gif");
        // make index.html the default page
        assets("/", "index.html");
        // prevent 404 errors due to browsers requesting favicons
        get("/favicon.ico", () -> Results.noContent());
    }
}
